<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Youtube;
use App\Form\YoutubeFormType;
use App\Repository\YoutubeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class YoutubeController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var YoutubeRepository
     */
    private YoutubeRepository $youtubeRepository;

    public function __construct(EntityManagerInterface $entityManager, YoutubeRepository $youtubeRepository)
    {
        $this->entityManager = $entityManager;
        $this->youtubeRepository = $youtubeRepository;
    }

    /**
     * @Route("/", name="app_home")
     */
    public function index(Request $request): Response
    {
        $youtube = new Youtube();
        $form = $this->createForm(YoutubeFormType::class, $youtube);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($youtube);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_home');
        }

        return $this->render('youtube/index.html.twig', [
            'form' => $form->createView(),
            'youtubeList' => $this->youtubeRepository->findAll()
        ]);
    }

    /**
     * @Route("/{id}", name="app_video")
     */
    public function video(Youtube $youtube): Response
    {
        return $this->render('youtube/video.html.twig', [
            'youtube' => $youtube,
        ]);
    }
}
